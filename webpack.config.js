const path = require('path');
module.exports = {
	mode: "production",
	entry: {
	lib: ["./node_modules/popper.js/dist/popper.min.js", "./node_modules/jquery/dist/jquery.slim.min.js", "./node_modules/bootstrap/dist/js/bootstrap.min.js"]        
	},
	output: {
		filename: "[name].bundle.js",
		path: path.resolve(__dirname, "public/js")
	}
};
