get_data()

// get products data
function get_data() {
	url='http://localhost:3000/api/furniture'
	fetch(url)
	.then ((response)=> {
	if (response.ok)
		return(response.json())
	else
		throw(`Error ${response.status}: ${response.statusText}\nWhile trying to fetch ${url}`)
	})
	.then (data => products_list(data))

	.catch ((error) => {
		console.log(error)

	})
}

function html_to_element(html) {
	template=document.createElement('template')
	template.innerHTML= html
	return (template.content.firstChild)
}

// redirect to the product page
function product_page(dat, event) {
    event.stopPropagation()
	location = `./produit.html?id=${dat._id}`
}
// display products on the page and add click event listener
function products_list (data) {
	row=document.getElementById('products')
	template=html_to_element('\
<li class="p-3 col-12 col-lg-6">\
		<div class="border border-primary shadow">\
				<img class="card-img-top">\
				<div class="card-body">\
						<h5 class="card-title"></h5>\
						<p class="card-text"></p>\
						<p class="card-text price"></p>\
				</div>\
		</div>\
</li>\
	')
	for (let dat of data) {
		clone=template.cloneNode(true)
		clone.querySelector('.card-img-top').setAttribute('src', dat.imageUrl)
		clone.querySelector('.card-img-top').setAttribute('alt', dat.name)
		clone.querySelector('.card-title').textContent = dat.name
		clone.querySelector('.card-text').textContent = dat.description
		clone.querySelector('.price').textContent = 'Prix: ' + dat.price / 100
		clone.addEventListener('click', product_page.bind(event, dat))
		row.appendChild(clone)
	}
}
